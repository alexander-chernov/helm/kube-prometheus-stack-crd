#!/bin/zsh
set -e
VERSION="kube-prometheus-stack-40.0.0"

rm -f templates/*.yaml
git clone -b "$VERSION" --depth=1 https://github.com/prometheus-community/helm-charts.git
mv helm-charts/charts/kube-prometheus-stack/crds/*.yaml templates/
rm -rf helm-charts
