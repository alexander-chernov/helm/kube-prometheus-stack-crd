# Kube Prometheus Stack Crd

Due to incompatibility of ArgoCD and Prometheus helm charts, I've created this repo which contains only the CRD files from 
prometheus

The only difference is the application of `argocd.argoproj.io/sync-options: Replace=true` annotation

Chart will be updated only on change of related CRD files